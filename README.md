# Blood Cancer detection

The purpose of our project is to develop a system that can automatically detect cancer from blood cell images. This system uses a convolution network that inputs a blood cell images and outputs whether the cell is infected with cancer or not. 